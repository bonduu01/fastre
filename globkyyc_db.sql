-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Sep 26, 2021 at 12:53 PM
-- Server version: 10.3.31-MariaDB-log-cll-lve
-- PHP Version: 7.3.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `globkyyc_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `id` bigint(20) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `comment` varchar(300) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tracking`
--

CREATE TABLE `tracking` (
  `id` int(100) NOT NULL COMMENT 'Primary key',
  `track_number` varchar(200) NOT NULL,
  `ship_date` varchar(100) DEFAULT NULL,
  `delivery_date` varchar(100) DEFAULT NULL,
  `ship_fname` varchar(100) DEFAULT NULL,
  `ship_flocation` varchar(100) DEFAULT NULL,
  `ship_tname` varchar(100) DEFAULT NULL,
  `ship_tlocation` varchar(100) DEFAULT NULL,
  `weight` varchar(100) DEFAULT 'Unspecified',
  `package` varchar(200) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `service` varchar(100) DEFAULT NULL,
  `progress1` varchar(100) DEFAULT NULL,
  `date1` varchar(100) DEFAULT NULL,
  `progress2` varchar(100) DEFAULT NULL,
  `date2` varchar(100) DEFAULT NULL,
  `progress3` varchar(100) DEFAULT NULL,
  `date3` varchar(30) DEFAULT NULL,
  `progress4` varchar(100) DEFAULT NULL,
  `date4` varchar(30) DEFAULT NULL,
  `progress5` varchar(100) DEFAULT NULL,
  `date5` varchar(25) DEFAULT NULL,
  `progress6` varchar(100) DEFAULT NULL,
  `date6` varchar(25) DEFAULT NULL,
  `progress7` varchar(100) DEFAULT NULL,
  `date7` varchar(25) DEFAULT NULL,
  `progress8` varchar(100) DEFAULT NULL,
  `date8` varchar(25) DEFAULT NULL,
  `progress9` varchar(100) DEFAULT NULL,
  `date9` varchar(25) DEFAULT NULL,
  `progress10` varchar(100) DEFAULT NULL,
  `date10` varchar(25) DEFAULT NULL,
  `user` varchar(50) DEFAULT 'superuser'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(100) NOT NULL,
  `username` varchar(25) NOT NULL,
  `password` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`) VALUES
(1, 'superuser', '123456'),
(2, 'user', '123456');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tracking`
--
ALTER TABLE `tracking`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tracking`
--
ALTER TABLE `tracking`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT COMMENT 'Primary key';

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
